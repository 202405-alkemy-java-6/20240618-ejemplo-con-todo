package cl.clases.yyy.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.clases.yyy.model.Usuario;
import cl.clases.yyy.repo.IUsuarioRepo;
import cl.clases.yyy.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepo repo;
	
	@Override
	public boolean isLoginValido(String username, String password) {
		
		Optional<Usuario> user = repo.obtenerPorUsuernameAndPassword(username, password);
		return user.isPresent(); //si encontro usuario, isPresent() es TRUE, en caso contrario es FALSE
		
	}
	
}
