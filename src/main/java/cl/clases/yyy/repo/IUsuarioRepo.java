package cl.clases.yyy.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cl.clases.yyy.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>{

	@Query("SELECT u FROM Usuario u WHERE u.username = :username AND u.password = :password ")
	Optional<Usuario> obtenerPorUsuernameAndPassword(@Param("username") String username,@Param("password") String password);
	
}
