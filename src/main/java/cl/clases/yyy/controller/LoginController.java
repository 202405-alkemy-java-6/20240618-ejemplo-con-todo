package cl.clases.yyy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.clases.yyy.dto.request.LoginDtoRequest;
import cl.clases.yyy.service.IUsuarioService;

@Controller
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping("/volver")
	public String volverAlLogin(Model modelo) {
		
		return "login";
		
	}
	
	@PostMapping("/validar")
	public String validar(Model modelo, @ModelAttribute LoginDtoRequest request) {

		// SI NO VIENE ALGUN DATO, RECHAZAMOS
		if(request.getUser() == null || request.getPass() == null){
			modelo.addAttribute("mensaje", "Debe ingresar usuario y password!");
			return "error";
		}
		
		// SI EL LOGIN NO ES VALIDO, RECHAZAMOS
		if(!usuarioService.isLoginValido(request.getUser(), request.getPass())){

			modelo.addAttribute("mensaje", "Usuario no éxiste");
			return "error";
			
		}
		
		// SI TODO PASO BIEN; ENTONCES SEGUIMOS AL HOME;
		modelo.addAttribute("mensaje", "Usuario logeado con éxito");
		return "home";
		
		
	}
	
}
